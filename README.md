# clipstore

Simple mechanism for persisting clipboard contents. The idea is for it to be used similarly to vim registers, where you store the current clipboard into registers, and can restore the clipboard from those registers.

## Status

I was able to get the functionality I wanted with [copyq](https://copyq.readthedocs.io) with its scripting capabilities. As such, I am no longer working on this. 