#!/usr/bin/env python3

import argparse
import pathlib
import socket
import sys
from typing import Optional

import msgpack

from .common import Action, K, get_socket_path


class ResponseError(Exception):
    def __init__(self, response):
        super().__init__(response[K.ERROR])
        self.response = response


class Client:
    def __init__(self):
        self.sock = socket.socket(socket.AF_UNIX)
        self.sock.connect(get_socket_path())

    def _sendmsg(self, msg: dict):
        write = self.sock.makefile("wb")
        msgpack.pack(msg, write)
        write.close()
        self.sock.shutdown(socket.SHUT_WR)

        read = self.sock.makefile("rb")
        rslt = msgpack.unpack(read)
        read.close()
        self.sock.shutdown(socket.SHUT_RD)
        if K.ERROR in rslt:
            raise ResponseError(rslt)
        return rslt

    def write(self, key: str, /, file: pathlib.Path = None, value: str = None):
        msg = {K.ACTION: Action.WRITE, K.KEY: key}
        if file and value is not None:
            raise ValueError("Cannot supply both value and file path")

        if value is not None:
            msg[K.DATA] = value.encode("utf8")
        elif file:
            msg[K.FILE_PATH] = str(file.absolute())
        else:
            # TODO: use pipe, or similar to pass data?
            msg[K.DATA] = sys.stdin.buffer.read()

        self._sendmsg(msg)

    def read(self, key: str, /, file: Optional[pathlib.Path] = None):
        msg = {K.ACTION: Action.READ, K.KEY: key}
        if file:
            msg[K.FILE_PATH] = str(file.absolute())

        result = self._sendmsg(msg)
        if K.RESULT in result:
            sys.stdout.buffer.write(result[K.RESULT])

    def list(self):
        result = self._sendmsg({K.ACTION: Action.LIST})
        for k in result[K.RESULT]:
            print(k)

    def delete(self, key: str):
        rslt = self._sendmsg({K.ACTION: Action.DELETE, K.KEY: key})
        if not rslt[K.RESULT]:
            print("Key does not exist.", file=sys.stderr)
            sys.exit(1)

    def __enter__(self):
        return self

    def __exit__(self):
        self.sock.close()


def parse_args():
    keyed_parser = argparse.ArgumentParser(add_help=False)
    keyed_parser.add_argument("key")

    parser = argparse.ArgumentParser(
        prog="clipstore", description="Stash clipboard contents in named slots"
    )
    subcommands = parser.add_subparsers(
        help="command",
        dest="operation",
        required=True,
    )
    read_parser = subcommands.add_parser(
        "read",
        help="read the value of a stowd clipboard to stdout",
        parents=[keyed_parser],
    )
    read_parser.add_argument(
        "--file",
        "-f",
        type=pathlib.Path,
        help="Write result in specified file instead of stdout",
    )

    write_parser = subcommands.add_parser(
        "write",
        help="write value from stdin with the given key",
        parents=[keyed_parser],
    )
    write_parser.add_argument(
        "--file",
        "-f",
        type=pathlib.Path,
        help="Read value from specified file instead of stdin",
    )
    write_parser.add_argument(
        "--value",
        "-v",
        type=str,
        help="Specify the value as an argument on the command line. Conflicts with --file.",
    )

    subcommands.add_parser(
        "delete", help="delete value with the given key", parents=[keyed_parser]
    )
    subcommands.add_parser(
        "list",
        help="List available keys",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    client = Client()
    if args.operation == "read":
        client.read(args.key, file=args.file)
    elif args.operation == "write":
        client.write(args.key, file=args.file, value=args.value)
    elif args.operation == "list":
        client.list()
    elif args.operation == "delete":
        client.delete(args.key)


if __name__ == "__main__":
    main()
