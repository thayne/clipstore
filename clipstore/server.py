#!/usr/bin/env python3
"""
Daemon for managing the clipstore.

This is a persistent daemon which listens on a (unix domain) socket and will
store and retrieve snippets of data, such as clipboard info, stored with a key.
"""

import os
import socketserver

import msgpack

from .common import Action, K, get_socket_path

# TODO: make this configurable
SOCK_PATH = get_socket_path()


class MessageException(Exception):
    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def error_response(self) -> dict:
        "Return an object that can be serialized as a response to return the error to the client"

        return {
            K.ERROR: self.msg,
        }


class ClipstoreHandler(socketserver.StreamRequestHandler):
    "Class for handling requests from the socket"

    def setup(self):
        super().setup()
        self._actions = {
            Action.READ: self._read,
            Action.WRITE: self._write,
            Action.LIST: self._list,
            Action.DELETE: self._delete,
        }

    def handle(self):
        try:
            self._handle_request()
        except MessageException as exc:
            msgpack.pack(exc.error_response(), self.wfile)

    def _handle_request(self):
        try:
            # TODO: support json and/or cbor as well somehow?
            # How do we determine which protocol is in use? a separate header?
            # maybe read into bytes, and try each
            msg = msgpack.unpack(self.rfile)
            # If no action is given default to the READ action
            action = msg.get(K.ACTION, Action.READ)
        except msgpack.exceptions.UnpackException:
            raise MessageException("Message is not valid msgpack")
        except Exception as e:
            raise MessageException(f"Request failed: {e}")

        try:
            action_fun = self._actions[action]
        except KeyError as err:
            raise MessageException(f"Unknown action: {err}")

        try:
            action_fun(msg)
        except FileNotFoundError as e:
            raise MessageException(f"File does not exist: {e.filename}")
        # TODO: handle other types of errors?

    def _read(self, msg: dict):
        key = msg[K.KEY]
        value = self.server.store.get(key, b"")
        file_path = self._get_file_path(msg)
        if file_path is not None:
            with open(file_path, "wb") as f:
                f.write(value)
            resp = {}
        else:
            resp = {K.RESULT: value}
        msgpack.pack(resp, self.wfile)

    def _write(self, msg: dict):
        key = msg[K.KEY]
        file_path = self._get_file_path(msg)
        if file_path is not None:
            with open(file_path, "rb") as f:
                value = f.read()
        else:
            # TODO: DATA_BASE64?
            value = msg[K.DATA]
        # mime_type = msg.get(K.MIME_TYPE, "text/plain")
        # volatile = msg.get(K.VOLATILE, False)

        self.server.store[key] = value

        msgpack.pack({}, self.wfile)

    def _list(self, _msg: dict):
        # TODO: option to include values
        msgpack.pack({K.RESULT: list(self.server.store.keys())}, self.wfile)

    def _delete(self, msg: dict):
        key = msg[K.KEY]
        deleted = False
        try:
            del self.server.store[key]
            print(f"store={self.server.store}")
            deleted = True
        except KeyError:
            pass
        msgpack.pack({K.RESULT: deleted}, self.wfile)

    def _get_file_path(self, msg: dict):
        path = msg.get(K.FILE_PATH, None)
        if path is not None and not os.path.isabs(path):
            raise ValueError("File path must be absolute")
        return path


def start_server():
    "Listen on the socket, and start the main loop"
    try:
        os.remove(SOCK_PATH)
    except FileNotFoundError:
        pass
    # TODO: systemd socket activation
    with socketserver.UnixStreamServer(SOCK_PATH, ClipstoreHandler) as server:
        # for now, just use a dictionary
        server.store = dict()
        server.serve_forever()


if __name__ == "__main__":
    start_server()
