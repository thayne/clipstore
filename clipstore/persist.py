import sqlite3

DB_VERSION=1

class Store:
    def __init__(self, conn):
        self._conn = conn

    def create_db(self):
        # If this changes, we may need to make use of the user_version pragma
        self._conn.execute('PRAGMA main.auto_vacuum=1')
        self._conn.execute('CREATE TABLE IF NOT EXISTS entries(name TEXT PRIMARY KEY, mime TEXT, content) WITHOUT ROWID')

    def store(self, name, mime, content):
        self._conn.execute('INSERT OR REPLACE INTO entries (name, mime, content) VALUES (?, ?, ?)', (name, mime, content))

    def retrieve(self, name):
        with self._conn.cursor() as cur:
            cur.execute('SELECT mime, content FROM entries WHERE name=?', (name,))
            return next(cur)
