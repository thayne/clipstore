"""
Common functionality shared between the server and client
"""

import enum
import os
from os import path

SOCKET_ENV_VAR = "CLIPSTORE_SOCKET"


# Action constants
class Action(enum.StrEnum):
    READ = "r"
    WRITE = "w"
    LIST = "l"
    DELETE = "d"


# attribute names
class K(enum.StrEnum):
    ACTION = "a"
    KEY = "k"
    FILE_PATH = "f"
    DATA = "d"
    DATA_BASE64 = "b"
    TRANSIENT = "s"
    MIME_TYPE = "t"
    ERROR = "e"
    RESULT = "r"


def get_socket_path():
    sockpath = os.environ.get(SOCKET_ENV_VAR, None)
    if sockpath is None:
        sockpath = path.join(os.environ["XDG_RUNTIME_DIR"], "clipstore.socket")
    return sockpath
